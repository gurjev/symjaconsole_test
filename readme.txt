 
  SymjaConsole - Java Symbolic Math System
========================================== 

The console app and JSR223 implementation of the Symja Computer Algebra System.

Run the console with the command:
java -jar symja-console-YYYY-MM-DD.jar

Online demo: 
* http://symjaweb.appspot.com/

See the Wiki pages:
* https://bitbucket.org/axelclk/symja_android_library/wiki
	
axelclk_AT_gmail_DOT_com 